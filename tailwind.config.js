/** @type {import('tailwindcss').Config} */
// export default {
//   content: [],
//   theme: {
//     extend: {},
//   },
//   plugins: [],
// }
module.exports = {
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    extend: {
      backgroundImage: {
        'home': "url('./src/assets/home.svg')",
        'trip1': "url('./src/assets/trip1.svg')",
        'trip2': "url('./src/assets/trip2.svg')",
        'trip3': "url('./src/assets/trip3.svg')",
        'DownloadApp': "url('./src/assets/DownloadApp.svg')",
      },
      spacing: {
        '30': '30px',
        '32':'32px'
      },
      borderRadius: {
        'rounded-5':'5px'
      }
    },
    colors: {
      'primary':'#2F80ED',
      'gray':'rgba(51, 51, 51, 1)',
      'black':'181818',
      'white':'#fff',
      'secondary':'#F2F2F2'
}
  },
  plugins: [],
}

