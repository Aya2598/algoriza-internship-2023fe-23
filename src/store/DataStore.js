import { defineStore } from 'pinia';

export const useDataStore = defineStore({
  id: 'data',
  state: () => ({
   hotels: JSON.parse(localStorage.getItem('arrayOfHotels')) || [],
  }),
  actions: {
    setHotels(array) {
      this.hotels = array;
      localStorage.setItem('arrayOfHotels', JSON.stringify(array.hotels));
    },
    getHotels() {
      return this.hotels;
    },
  },
});
