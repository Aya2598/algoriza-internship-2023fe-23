import instance from "./baseInstance";
export const getHotelDetaile = (data) => {
    return instance.get("getHotelDetails", {
      params: data
    });
  };