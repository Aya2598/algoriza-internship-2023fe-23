import instance from "./baseInstance";
export const getSortByOptions = (data) => {
    return instance.get("getSortBy", {
      params: data
    });
  };