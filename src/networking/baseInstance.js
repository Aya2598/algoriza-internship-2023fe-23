import axios from 'axios';
const instance = axios.create({
  baseURL: 'https://booking-com15.p.rapidapi.com/api/v1/hotels/',
  headers:  {
    'X-RapidAPI-Key': 'd928acbfcemsha1ad238726c57fdp1798bbjsne7865724d633',
    'X-RapidAPI-Host': 'booking-com15.p.rapidapi.com'
  }
});
export default instance;