
// import fs from 'fs'; // Assuming Node.js environment to read JSON file
// import jwt from 'jsonwebtoken';

// const usersData = JSON.parse(fs.readFileSync('./users.json')); // Read users from JSON file

// const secretKey = 'your_secret_key_here'; // Replace with your secret key

// export const login = async (email, password) => {
//   try {
//     const user = usersData.find(u => u.email === email && u.password === password);
//     if (!user) throw new Error('Invalid email or password');

//     const token = jwt.sign({ userId: user.id, email: user.email }, secretKey, { expiresIn: '1m' });
//     return { token };
//   } catch (error) {
//     throw new Error(error.message);
//   }
// };

// import users from './users.json';

// export function login(email, password) {
//   const user = users.find(user => user.email === email && user.password === password);
//   return user ? user.uuid : null;
// }
import usersData from '../assets/users.json';
import axios from 'axios';
export function getUsers() {
  console.log(usersData)
  return usersData;
}

export const addUser =async(newUser) =>{
  // usersData.push(newUser);
  try {
    console.log(usersData)
    console.log("lkk")
    usersData.push(newUser); // Add new user data to the existing list
    await axios.put('../assets/users.json',usersData); // Replace with your file path
    
  } catch (error) {
    console.error('Error updating user data:', error);
  }

}

export function findUserByEmail(email) {
  return usersData.find(user => user.email === email);
}
