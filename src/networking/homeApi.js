import instance from "./baseInstance";
export const getDestinations = () => {
  return instance.get("searchDestination", {
    params: { query: "Egypt" }
  });
};


export const searchHotels = (data) => {
    return instance.get("searchHotels", {
      params: data
    });
  };



// () => {
//     axios
//       .get(
//         "https://booking-com15.p.rapidapi.com/api/v1/hotels/searchDestination",
//         {
//           params: { query: "Egypt" },
//           headers: {
//             "X-RapidAPI-Key":
//               "9c9749b543mshae304e8a5e6019bp1a1b1fjsn9c718dcfa01b",
//             "X-RapidAPI-Host": "booking-com15.p.rapidapi.com",
//           },
//         }
//       )
//       .then((response) => {
//         destinations.value = response.data.data;
//       }).catch((e)=>{console.log(e)})
//   };