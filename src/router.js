// router/index.js
import { createRouter, createWebHistory } from 'vue-router'
import Home from './pages/Home.vue'
import Login from './pages/Login.vue'
import Register from './pages/Register.vue'
import SearchResult from './pages/SearchResult.vue'
import HotelDetaile from './pages/HotelDetaile.vue'
import Payment from './pages/Checkout.vue'
import Welcome from './pages/Welcome.vue'
import Trips from './pages/Trips.vue'
const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/welcome',
        name: 'Welcome',
        component: Welcome
    },
    {
        path: '/login',
        name: 'Login',
        component:Login
    }
    ,
    {
        path: '/register',
        name: 'Register',
        component:Register
    }
    ,
    {
        // path: '/searchresult/:data',
        path: '/searchresult',
        name: 'SearchResult',
        component:SearchResult,
        meta: { requiresAuth: true }
        
    }
    ,
    {
        path: '/hoteldetail/:id',
        name: 'HotelDetaile',
        component:HotelDetaile,
        
    }
    ,
    {
        path: '/payment',
        name: 'Payment',
        component:Payment,
        
    },
    {
        path: '/trips',
        name: 'Trips',
        component:Trips,
        
    }
]

const router = createRouter({ history: createWebHistory(), routes })
// router.beforeEach((to, from, next) => {
//     const loggedIn = checkLoggedIn(); // Implement this function to check user login status
//     if (to.meta.requiresAuth && !loggedIn) {
//       next('/login'); // Redirect to login page if trying to access a protected route without logging in
//     } else {
//       next('/searchresult/:data'); // Proceed to the requested route
//     }
//   });
//   function checkLoggedIn() {
//     const user = JSON.parse(localStorage.getItem('userData')); // Retrieve user data from local storage
//     return user !== null  && user.email !== undefined;; 
//   }

export default router
