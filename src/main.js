import { createApp } from 'vue'
import { createPinia } from 'pinia';
import './style.css'
import App from './App.vue'
import VueAwesomePaginate from "vue-awesome-paginate";
import "vue-awesome-paginate/dist/style.css";
import router from './router.js'
const app = createApp(App);
const pinia = createPinia();
app.use(pinia);
app.use(router);
app.use(VueAwesomePaginate);
app.mount('#app');
// createApp(App).mount('#app')
// createApp(App).use(router).mount('#app')